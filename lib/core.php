<?php

namespace PHPFrame\Core;

if(function_exists('app')){

  function app($key)
  {
    $App = Application::instance()->get($key);
    if(is_array($App)) $App = $App[0];
    return $App;
  }
  
}
