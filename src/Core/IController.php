<?php
namespace PHPFrame\Core;

use PHPFrame\Http\Request;
use PHPFrame\Http\Response;

interface IController
{
    public function setRequest(Request $request);
    public function setResponse(Response $response);
}