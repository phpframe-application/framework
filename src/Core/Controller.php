<?php

namespace PHPFrame\Core;

use PHPFrame\Http\Request;
use PHPFrame\Http\Response;
// use League\Container\Container;

abstract class Controller implements IController
{
    protected $request;
    protected $response;
    private $container;
    protected $template;

    public function __construct()
    {
        $this->container = Application::Instance();
        $this->request   = $this->container->get('request');
        $this->response  = $this->container->get('response');
        $this->template  = $this->container->get('template');
    }

    public function __invoke(Request $request, Response $response)
    {
        return $response;
    }

    public function setRequest(Request $request)
    {
        $this->request = $resquest;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    public function view($view, $data = [])
    {
        $view = str_replace(['.', '/', '\\'], DS, $view);
        $page = $this->template->render("$view.php", $data);
        $body = $this->response->getBody()->write( $page );
        return $this->response;
    }

}
