<?php

namespace PHPFrame\Core;

use League\Container\Container;

class Application extends Container
{
	private static $Instance;
	// protected $blueprint;

	public static function instance()
	{
		if(empty(static::$Instance)){
			static::$Instance = new Application();
		}
		return static::$Instance;
	}

	public function bind($key, $args, $params = null)
	{

		if(is_callable($args)) $args = $args();

		if($params == null){
			$this->share($key, $args);
		}
		elseif(is_bool($params) && $params == true){
			$this->add($key, $args);
		}
		else{
			$binder = $this->inflector($key);
			foreach($params as $k => $val){
				$binder->invokeMethod($k, $val);
			}
		}
		return $this;
	}

	public function routeRequest(Routing $router)
	{

	}

	public function respondWith(Routing $router)
	{

	}

	public function sendRequest(Request $request)
	{

	}
	
	public function generateCallTrace(Exception $e)
	{
		$trace = explode("\n", $e->getTraceAsString());
		// reverse array to make steps line up chronologically
		$trace = array_reverse($trace);
		array_shift($trace); // remove {main}
		array_pop($trace); // remove call to this method
		$length = count($trace);
		$result = array();

		for ($i = 0; $i < $length; $i++)
		{
			$result[] = ($i + 1)  . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
		}
        print '<div style="background-color:darkblue;color:white; line-height:1; padding:0.5rem">';
		print("<h1>".$e->getMessage()."</h1>");
		print("<h3>Line ".$e->getLine().": ".$e->getFile()." [Error code: ".$e->getCode()."]</h3>");
		print "</div><pre>";
		print "\t" . implode("\n\t", $result);
		print "</pre>";
	}

	public function run($start_time)
	{
		try{
		    $routing  = $this->get('routing');
		    $request  = $this->get('request');
		    $response = $this->get('response');

		    $viewable = $routing->dispatch( $request, $response);

		    $this->get('emitter')->emit( $viewable );
		}
		catch(\Exception $e){
		    print $this->generateCallTrace($e);
		}
		
		$time = microtime(true) - $start_time;
		dump($time); return $time;
	}
}
