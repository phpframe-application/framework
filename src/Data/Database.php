<?php

namespace PHPFrame\Data;

use RedBeanPHP\R;

/**
*
*/
class Database extends R
{
	private static $connection;

	private function __construct($dbConfigs)
	{
		if(is_string($dbConfigs)){
			return $this->setup($dbConfigs);
		}
		extract((array)$dbConfigs);		
		$dsn = "$type:host=$host;dbname=$name";
		return $this->setup($dsn, $user, $pass);
	}

	public static function connection($dbConfigs)
	{
		if(empty(static::$connection)){
			static::$connection = new Database($dbConfigs);
		}
		return static::$connection;
	}
}
