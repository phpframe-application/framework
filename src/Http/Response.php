<?php

namespace PHPFrame\Http;

use Zend\Diactoros\Response as ZendResponse;

class Response extends ZendResponse
{
	private $content;

	public function setContent($content)
	{
		$this->content = $content;
	}

	public function getContent()
	{
		return $this->content;
	}
}
