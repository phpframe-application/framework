<?php

namespace PHPFrame\Http;

use PHPFrame\Core\Application;
use League\Route\RouteCollection;

class Routing extends RouteCollection
{

	public function __construct(Application $Application, $routes = [])
	{
		parent::__construct($Application);
		$this->process($routes);
	}
	protected $_routes, $application;

	public function process($routes = [])
	{
		foreach ($routes as $route) {
			$this->_routes[] = require $route;
		}

		foreach (array_filter($this->_routes) as $routes) {

			$this->makeRoutes($routes);
		}
		return $this;
	}

	public function makeRoutes($routes)
	{
		foreach ($routes as $name => $route) {

			$url = $route['url'] ?: $route[0];

			$method = $route['method'] ?? $route[1];
			if(strtoupper($method) == 'ANY'){
				$method = ['GET','POST','PUT','PATCH','DELETE'];
			}

			$callable = $route['callable'] ?? $route['controller'] ?? $route[2];

			$this->map($method, $url, $callable);
		}
		return $this;
	}


	public function respond()
	{
		$request  = $this->container->get('request');
		$response = $this->container->get('response');
		return $this->dispatch($request, $response);
	}
}
